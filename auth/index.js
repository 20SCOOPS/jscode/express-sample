var app = require('express')
var bodyParser = require('body-parser');
var axios = require('axios')

var port = process.env.PORT || 7001
var log = (req, res, next) => {
    console.log(req.url)
    next()
}
app()
//set use body json parser
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({extended: true}))
    .use(log)
    .get('/auth', (req, res) => {
        res.send({
            id:req.query.id,
            desc:"well hello",
            method:'GET'
        })
    })
    .get('/auth/:id/gender', (req, res) => {
        res.send({
            id:req.params.id,
            url:req.url,
            gender:'male',
            method:'GET'
        })
    })
    .post('/auth', (req, res) => {
        res.send({
            name: req.body.name,
            method:'POST'
        })
    })
    .put('/auth', (req, res) => {
        res.send({
            name: req.body.name,
            method:'PUT'
        })
    })
    .listen(port, () => {
        console.log('Starting node.js on port ' + port)
    })