var app = require('express')
var bodyParser = require('body-parser');
var axios = require('axios')

require('dotenv').config('./.env')

var port = process.env.PORT || 7000

var redirect = to => (req, res) => {
    console.log(req.body)
    axios({
        method : req.method,
        url : req.url.replace(new RegExp(req.route.path), to),
        data : req.body,
        headers : req.headers
    })
    .then((response) => {
        res.send(response.data)
    })
    .catch((error) => {
        res.status(500).send({ error })
    })
}
var routeWrap = (api) => `/api/${api}($|/*)`

var log = (req, res, next) => {
    console.log(req.url)
    next()
}

var checkAuth = (req, res, next) => {
    if (req.headers.authorization) {
        next()
    }
    else {
        res.status(403).send({ error: 'Authentication Failed' })
    }
}

var notFound = (req, res, next) => {
    res.status(404).send({ error: 'Service not found' })
}

app()
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({ extended: true }))

    .use(log)
    .use(routeWrap('auth'), checkAuth)
    .all(routeWrap('auth'), redirect(process.env.AUTH_SERVICE))

    .use(notFound)
    .listen(process.env.PORT, '0.0.0.0', err => console.log('Starting node.js on port ' + process.env.PORT + ' : ' + (err || 'success')))